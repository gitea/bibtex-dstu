#!/bin/bash

REPO_ROOT_PATH="$( realpath "$( dirname "$0" )" )"
cd "$REPO_ROOT_PATH"

# Clean build directory
[ -a ./build ] && rm -rf ./build
[ -f ./dstu.zip ] && rm ./dstu.zip
mkdir ./build/

pushd ./build/
ln -s "$REPO_ROOT_PATH"/source/bibtex/dstu/dstu.dtx dstu.dtx

exit_if_fail () {
    STATUS="$?"
    echo "$1"
    exit "$STATUS"
}

pdflatex dstu.dtx \
&& makeindex -r -s gind.ist dstu \
&& makeindex -r -s gglo.ist -o dstu.gls dstu.glo \
&& pdflatex dstu.dtx \
&& pdflatex dstu.dtx \
|| exit_if_fail "'dstu.dtx' build failed!"

mkdir -p ./doc/bibtex/dstu/ ./bibtex/bst/dstu/ \
&& mv *.bst ./bibtex/bst/dstu/ \
&& mv dstu.pdf ./doc/bibtex/dstu/ \
&& cp ./dstu.dtx ./doc/bibtex/dstu/ \
|| exit_if_fail 'Moving built files to their folders failed!'

if [[ -n "$DSTU_NO_EXAMPLES" ]] ;
then
    echo "Examples' build is disabled. Exitting."
    exit
fi

# To let `bibtex*` know where to search for styles.
export BSTINPUTS="$(pwd)"/bibtex/bst/dstu/

# Build utf8 examples.
mkdir -p ./doc/bibtex/dstu/examples/utf8/
pushd ./doc/bibtex/dstu/examples/utf8/

for F in "$REPO_ROOT_PATH"/doc/bibtex/dstu/examples/utf8/{dstu.tex,udstu2015*.tex,uudstu2015*.tex,bib}
do
    ln -s "$F" ./
done

for F in udstu2015*.tex uudstu2015*.tex ; do
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 1 failed."
done

CSF_PATH="$(kpsewhich -var-value=TEXMFDIST)/bibtex/csf/gost/utf8cyrillic.csf"
for F in udstu2015*.aux ; do
    bibtex8 --debug=search -B -c "$CSF_PATH" "$F" \
    || echo "\`bibtex8\` failed for ${F}."
done

for F in uudstu2015*.aux ; do
    bibtexu --debug=search -B -l ukr -o ukr "$F" \
    || {
        STATUS="$?"
        echo "\`bibtexu\` failed for ${F}."

        # `bibtexu` often fails with segmentation fault, thus producing
        # truncated outputs. Cleaning up in these cases.
        [[ "$STATUS" -ge 128 ]] && mv "${F%.aux}.bbl" "${F%.aux}.bbl-failed"
    }
done

for F in udstu2015*.tex uudstu2015*.tex; do
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 2 failed."
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 3 failed."
done

popd

# Build cp1251 examples.
mkdir -p ./doc/bibtex/dstu/examples/cp1251/
pushd ./doc/bibtex/dstu/examples/cp1251/

for F in "$REPO_ROOT_PATH"/doc/bibtex/dstu/examples/utf8/dstu*.tex
do
    sed '/\\RequirePackage\[utf8\]{inputenc}/s/utf8/cp1251/' -- "$F" \
    | iconv -t cp1251 > "$(basename "$F")"
done

mkdir ./bib/
for F in "$REPO_ROOT_PATH"/doc/bibtex/dstu/examples/utf8/bib/*.bib
do
    iconv -t cp1251 -- "$F" > bib/"$(basename "$F")"
done

for F in dstu2015*.tex ; do
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 1 failed."
done

CSF_PATH="$(kpsewhich -var-value=TEXMFDIST)/bibtex/csf/gost/cp1251.csf"
for F in dstu2015*.aux ; do
    bibtex8 --debug=search -B -c "$CSF_PATH" "$F" \
    || echo "\`bibtex8\` failed for ${F}."
done

for F in dstu2015*.tex ; do
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 2 failed."
    pdflatex "$F" || exit_if_fail "Building ${F} at stage 3 failed."
done

popd

# Bundle build results.
rm ../dstu.zip 2>/dev/null
zip --compression-method store --no-dir-entries --no-wild -X \
    ../dstu.zip \
    -- \
    ./doc/bibtex/dstu/{dstu.pdf,dstu.dtx} \
    ./doc/bibtex/dstu/examples/{cp1251,utf8}/*.{tex,pdf} \
    ./doc/bibtex/dstu/examples/{cp1251,utf8}/bib/*.bib \
    ./bibtex/bst/dstu/*.bst
