# DSTU
BibTEX support for DSTU standard bibliographies
-------------------------------------------------------------------
DSTU is a bundle of BibTEX styles to format references according to
the DSTU 8302:2015.

This bundle is heavily based on the GOST bundle and share most of its
codebase. Generation of files from the original GOST bundle is removed
from the .ins file to avoid file name overlapping.

For copyright notice see preambles in corresponding files.

For usage instructions see `dstu.pdf` generated from `dstu.dtx`.

Happy BibTeXing!
